from rest_framework import permissions

class IsOwner(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return obj.user == request.user

class IsUser(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return obj == request.user

class IsUserOrStaff(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return obj == request.user or request.user.is_staff