
from django.contrib.auth.models import Group
from django.contrib import admin as admin
from user import UserAdmin
from dashboard.models.user import User

admin.site.register(User, UserAdmin)


# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)
