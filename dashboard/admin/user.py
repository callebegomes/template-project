############
# Esse arquivo se trata da configuracao do admin do django.
# Nao confundir com a aplicacao admin criada no projeto
############

from django.contrib.auth.admin import UserAdmin

from dashboard.forms.user import UserCreationForm, UserChangeForm

class UserAdmin(UserAdmin):
	# The forms to add and change user instances
	form = UserChangeForm
	add_form = UserCreationForm

	# The fields to be used in displaying the User model.
	# These override the definitions on the base UserAdmin
	# that reference specific fields on auth.User.
	list_display = ('username', 'email', 'is_active', 'is_staff', 'is_admin')
	list_filter = ('is_admin',)
	fieldsets = (
		(None, {'fields': ('username', 'email', 'password')}),
		('Permissions', {'fields': ('is_active','is_staff','is_admin',)}),
	)
	# add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
	# overrides get_fieldsets to use this attribute when creating a user.
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('username', 'email', 'password1', 'password2')}
		),
	)
	search_fields = ('username', 'email',)
	ordering = ('username', 'email',)
	filter_horizontal = ()

