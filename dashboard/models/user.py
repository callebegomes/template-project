# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not username:
            raise ValueError(u'nome de usuário obrigatório')
        if not email:
            raise ValueError(u'email obrigatório')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(username, email, password)
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    username = models.CharField(u'Nome de Usuário', max_length=34, unique=True)
    email = models.EmailField(u'Email', max_length=255, unique=True)
	
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
	
    objects = UserManager()
	
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email',]
	
    def get_full_name(self):
        return self.username
		
    def get_short_name(self):
        return self.username

    def __unicode__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @classmethod
    def get_user(cls, user_id):
        user = None
        try:
            user = User.objects.get(username=user_id)
        except User.DoesNotExist:
            try:
                user = User.objects.get(email=user_id)
            except User.DoesNotExist:
                pass
        return user
		
    class Meta:
        app_label = 'dashboard'
        verbose_name = u'Usuário'
        verbose_name_plural = u'Usuários'
# Create your models here.
