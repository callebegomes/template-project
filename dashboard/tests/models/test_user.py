import datetime
from django.test import TestCase
from dashboard.models.user import User

class UserTestCase(TestCase):

	def setUp(self):
		user = User(username='testuser', email='testuser@domain.com')
		user.set_password('testpassword');
		user.save()

	def test_get_user_class_method_should_return_user_from_username(self):
		user = User.objects.get(username='testuser')
		user2 = User.get_user('testuser')
		self.assertEqual(user, user2)

	def test_get_user_class_method_should_return_user_from_email(self):
		user = User.objects.get(username='testuser')
		user2 = User.get_user('testuser@domain.com')
		self.assertEqual(user, user2)

