from django.conf import settings
from django.contrib.auth import authenticate
from dashboard.models import User

class ModelBackend(object):

	def authenticate(self, username=None, password=None, request=None):
		user = None
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			user = None
			try:
				user = User.objects.get(email=username)
			except User.DoesNotExist:
				user = None
			
		if user and user.check_password(password):
			user = authenticate(username=user.username, password=password, request=request)
			return user
		return None

	def get_user(self, user_id):
		try:
			return User.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None
