/**
* Método que indica o primeiro indice do elemento procurado
*
* @valueOrPredicate - Valor a ser procurado ou método que recebe um elemento e
*   retorna true se for o elemento procurado
* @from - Indice para iniciar busca
*
* Retorna o primeiro indice encontrado ou -1 caso não encontre o elemento.
**/
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (valueOrPredicate /*, from*/) {
        var len = this.length >>> 0,
        underlyingArray = this,
        predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; },
        from = Number(arguments[1]) || 0;

        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0) from += len;

        for (; from < len; from++) {
            if (from in this && predicate(underlyingArray[from])) return from;
        }

        return -1;
    };
}

/**
* Método para instanciar um array de tamanho fixo e populado com valores padrões
*
* @length - indica o tamanho inicial do array a ser instanciado
* @def - indica o valor default a ser utilizado no novo array, ou uma função que
*   recebe o indice do elemento e retorna o valor default para o indice
*
* retorna o novo array construido e preenchido
**/
if(!Array.build){
    Array.build = function (length, def) {
        var value = def;
        length = parseInt(length);
        if(typeof def !== "function") value = function() {return def;};
        var n = [];
        for(var i = 0; !isNaN(length) && i < length; i++){
            n.push(def(i));
        }
        return n;
    };
}

/**
* Método que indica se existe ou não um elemento no array
*
* @valueOrPredicate - valor a ser procurado, ou método que recebe um
*   item e retorna true se for o elemento procurado
*
* returna true caso encontre o elemento, false caso contrário
**/
if (!Array.prototype.has) {
    Array.prototype.has = function (valueOrPredicate) {
        var underlyingArray = this;
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                return true;
            }
        }
        return false;
    };
}

/**
* Método que seleciona atributos dos elementos do array
*
* @attrOrPredicate - indica o atributo a ser retornado, ou método
*   que recebe o elemento e retorna valor selecionado.
*
* retorna um novo array com os valores selecionados
**/
if (!Array.prototype.select) {
    Array.prototype.select = function (attrOrPredicate) {
        var underlyingArray = this;
        var selected = [];
        var predicate = function (value) { return value; };
        if (typeof attrOrPredicate === "function") predicate = attrOrPredicate;
        else if (typeof attrOrPredicate === "string") predicate = function (value) { return value[attrOrPredicate]; };

        for (var i = 0, ilen = underlyingArray.length; i < ilen; i++) {
            selected.push(predicate(underlyingArray[i]));
        }
        return selected;
    };
}

/**
* Método que modifica o array para eliminar duplicidade de elementos
*
* @attr - opicional que indica o atributo a ser utilizado para distinguir os
*   elementos. caso não seja passado, será considerado o proprio elemento.
*
* retorna o próprio array modificado;
**/
if (!Array.prototype.distinct) {
    Array.prototype.distinct = function (attr) {
        var underlyingArray = this;
        var findedValues = [];
        var predicate = function (value) { return findedValues.has(value); }
        if (typeof attr === "string") predicate = function (value) { return findedValues.has(function (v) { return v[attr] == value[attr]; }); };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                underlyingArray.splice(i--, 1);
            } else {
                findedValues.push(value);
            }
        }
        return underlyingArray;
    };
}

/**
* Método que filtra os elementos de acordo com o valor passado
*
* @valueOrPredicate - indica o valor a ser comparado como filtro, ou método
*   que recebe o elemento e retorna true se elemento for filtrado.
*
* retorna novo array com todos os elementos que passaram pelo filtro.
**/
if (!Array.prototype.where) {
    Array.prototype.where = function (valueOrPredicate) {
        var underlyingArray = this;
        var findedValues = [];
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                findedValues.push(value);
            }
        }
        return findedValues;
    };
}

/**
* Método que filtra os elementos de acordo com o valor passado e os remove do array original
*
* @valueOrPredicate - indica o valor a ser comparado como filtro, ou método
*   que recebe o elemento e retorna true se elemento for filtrado.
*
* retorna novo array com todos os elementos que passaram pelo filtro.
**/
if (!Array.prototype.extract) {
    Array.prototype.extract = function (valueOrPredicate) {
        var underlyingArray = this;
        var findedValues = [];
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                findedValues.push(value);
                underlyingArray.splice(i--, 1);
            }
        }
        return findedValues;
    };
}

/**
* Método que subistitue todos os elemetos filtrados por um novo elemento
*
* @valueOrPredicate - indica valor a ser utilizado como filtro, ou método que recebe
*   um elemento e retorna true se elemento passar por filtro.
* @newValue - indica o novo valor a ser utilizado no lugar dos elementos filtrados.
*
* retorna o próprio array modificado.
**/
if (!Array.prototype.replace) {
    Array.prototype.replace = function (valueOrPredicate, newValue) {
        var underlyingArray = this;
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                underlyingArray[i] = newValue;
            }
        }
        return underlyingArray;
    };
}

/**
* Método que remove todos os elementos filtrados
*
* @valueOrPredicate - indica valor a ser utilizado como filtro, ou método que
*   recebe um elemento e retorna true se elemento passar por filtro.
*
* retorna o próprio array modificado
**/
if (!Array.prototype.remove) {
    Array.prototype.remove = function (valueOrPredicate) {
        var underlyingArray = this;
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                underlyingArray.splice(i--, 1);
            }
        }
        return underlyingArray;
    };
}

/**
* Método que ordena o array
*
* @attrOrPredicate - indica o atributo a ser considerado na ordenação, ou método
*   que recebe um item e retorna o valor considerado na ordenação.
*
* retorna o próprio array modificado
**/
if (!Array.prototype.orderBy) {
    Array.prototype.orderBy = function (attrOrPredicate) {
        var predicate = null;
        if (typeof attrOrPredicate == "string") predicate = function (item) { return item[attrOrPredicate]; };
        else if (typeof attrOrPredicate == "function") predicate = attrOrPredicate;
        else throw "attrOrPredicate must be string or function";
        this.sort(function (a, b) {
            var val_a = predicate(a), val_b = predicate(b);
            if (val_a < val_b) return -1;
            if (val_a == val_b) return 0;
            return 1;
        });
        return this;
    };
}

/**
* Método que adiciona um elemento único na tela, isto é, se o elemento não existir no
* array ele é adicionado no final do array, se ele existir, o elemento já existente
* é substituído pelo novo elemento.
*
* @newItem - novo item a ser adicionado ou atualizado
* @attrOrPredicate - attributo utilizado para distinguir os elementos, ou método que
*   recebe um elemento e retorna true caso seja igual ao novo elemento
*
* retorna o próprio array modificado
**/
if (!Array.prototype.addUnique) {
    Array.prototype.addUnique = function (newItem, attrOrPredicate) {
        var underlyingArray = this;
        var predicate = function (value) { return value === newItem; };
        if (typeof attrOrPredicate === "string") predicate = function (value) { return value[attrOrPredicate] === newItem[attrOrPredicate]; };
        else if (typeof attrOrPredicate === "function") predicate = attrOrPredicate;
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                underlyingArray[i] = newItem;
                return underlyingArray;
            }
        }
        underlyingArray.push(newItem);
        return underlyingArray;
    };
}

/**
* Método para adicionar os elementos de um array em outro de forma única.
* Para isso, o método se utiliza do método addUnique.
*
* @itens - array contendo os elementos para serem adicionados
* @attrOrPredicate - attributo utilizado para distinguir os elementos, ou método que
*   recebe dois elementos e retorna true caso sejam iguais
**/
if (!Array.prototype.concatUnique) {
    Array.prototype.concatUnique = function (itens, attrOrPredicate) {
        var underlyingArray = this;
        var predicate = function (a, b) { return a === b; };
        if (typeof attrOrPredicate === "string") predicate = function (a, b) { return a[attrOrPredicate] === b[attrOrPredicate]; };
        else if (typeof attrOrPredicate === "function") predicate = attrOrPredicate;
        for (var i = 0; i < itens.length; i++) {
            underlyingArray.addUnique(itens[i], function (valor) { return predicate(valor, itens[i]); });
        }
        return underlyingArray;
    };
}

/**
* @valueOrPredicate - indica o valor a ser procurado no array, ou método
*   que recebe um elemento e retorna true caso seja o procurado
*
* retorna o elemento anterior ao valor encontrado,
* undefined se o valor encontrado estiver no indice '0'
* e null caso não encontre o valor.
**/
if (!Array.prototype.previewItem) {
    Array.prototype.previewItem = function (valueOrPredicate) {
        var underlyingArray = this;
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                if (i == 0) return undefined;
                return underlyingArray[i - 1];
            }
        }
        return null;
    };
}

/**
* @valueOrPredicate - indica o valor a ser procurado no array, ou método
*   que recebe um elemento e retorna true caso seja o procurado
*
* retorna o elemento seguinte ao valor encontrado,
* undefined se o valor encontrado estiver no indice 'length - 1'
* e null caso não encontre o valor.
**/
if (!Array.prototype.nextItem) {
    Array.prototype.nextItem = function (valueOrPredicate) {
        var underlyingArray = this;
        var predicate = typeof valueOrPredicate === "function" ? valueOrPredicate : function (value) { return value === valueOrPredicate; };
        for (var i = 0; i < underlyingArray.length; i++) {
            var value = underlyingArray[i];
            if (predicate(value)) {
                if (i == underlyingArray.length - 1) return undefined;
                return underlyingArray[i + 1];
            }
        }
        return null;
    };
}

/**
* Método que limpa um array eliminando todos os elementos.
**/
if (!Array.prototype.clear) {
    Array.prototype.clear = function () {
        this.splice(0, this.length);
        return this;
    };
}

//busca uma interseção entre os itens de 2 arrays
var intersect = function intersect(a, b) {
    var result = [];

    for (var i = 0, ilen = a.length; i < ilen; i++) {
        if (b.has(a[i])) result.push(a[i]);
    }

    return result;
},

range = function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    };
    if (typeof step == 'undefined') {
        step = 1;
    };
    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    };
    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    };
    return result;
};