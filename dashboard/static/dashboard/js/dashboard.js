/**
* @author: Callebe Gomes
**/


(function (angular) {
	"use strict";
	
	var dashboard = angular.module('dashboard', ['ngRoute']);
	
	dashboard.config(function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/dashboard/blank-page', { templateUrl: '/dashboard/templates/blank-page', controller: 'BlankPageCtrl' })
			.when('/dashboard/', { templateUrl: '/dashboard/templates/dashboard', controller: 'DashboardCtrl' });

		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
	});
	
	dashboard.controller('IndexCtrl', ['$scope', function IndexCtrl($scope) {
		console.log('IndexCtrl');
	}]);
	
	/**
	* Controlers criados apenas para exemplificar o uso das rotas.
	* O ideal é que esses scontrollers sejam colocados em seus próprios arquivos
	*/
	dashboard.controller('BlankPageCtrl', ['$scope', function BlankPageCtrl($scope) {
		console.log('BlankPageCtrl');
	}])
	.controller('DashboardCtrl', ['$scope', function DashboardCtrl($scope) {
		console.log('DashboardCtrl');
	}]);
	
} (angular));
