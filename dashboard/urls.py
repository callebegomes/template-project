from django.conf.urls import patterns, url, include
from django.contrib.auth.views import login, logout_then_login

from dashboard.views.index import index

urlpatterns = patterns('',
    url(r'^api/', include('dashboard.urls_api')),
    url(r'^templates/', include('dashboard.urls_templates')),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout_then_login, name='logout'),
    
    url(r'^', index, name='index'),
)
