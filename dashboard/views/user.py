from rest_framework import permissions, viewsets
from dashboard.models import User
from dashboard.serializers.user import UserSerializer
from dashboard.permissions import IsUserOrStaff

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('username')
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, IsUserOrStaff)

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.queryset
        return User.objects.filter(id=self.request.user.id)
