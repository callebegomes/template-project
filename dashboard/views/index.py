from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render
	
def index(request):
	if request.user.is_authenticated() and request.user.is_staff:
		return render(request, 'admin_base.html')
	return HttpResponseRedirect(reverse('home'))
