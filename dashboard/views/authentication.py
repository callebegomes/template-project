from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, logout
from django.http import HttpResponse
from dashboard.model_backend import ModelBackend
import json

@csrf_exempt
def api_login(request):
	data = json.loads(request.body)
	modelBackend = ModelBackend()
	user = modelBackend.authenticate(username=data['username'], password=data['password'], request=request)
	if user is not None and user.is_active:
		login(request, user)
		data = {
			'user' : {
				'id': user.id,
				'username': user.username,
				'email': user.email
			}
		}
		return HttpResponse(json.dumps(data), content_type='application/json')
	
	data = {
		'user' : None
	}
	return HttpResponse(json.dumps(data), content_type='application/json')

@csrf_exempt
def api_logout(request):
	logout(request);
	return HttpResponse('')
