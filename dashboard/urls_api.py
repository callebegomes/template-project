from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter
from dashboard.views.user import UserViewSet
from dashboard.views.authentication import api_login, api_logout

router = DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = patterns('',
	url(r'^', include(router.urls)),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	url(r'^login/', api_login, name='api-login'),
	url(r'^logout/', api_logout, name='api-logout'),
)
