from django.conf.urls import patterns, url
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

class AuthenticatedTemplateView(TemplateView):

	@method_decorator(login_required)
	@method_decorator(staff_member_required)
	def dispatch(self, *args, **kwargs):
		return super(AuthenticatedTemplateView, self).dispatch(*args, **kwargs)	

urlpatterns = patterns('',
	url(r'^dashboard', AuthenticatedTemplateView.as_view(template_name='angular_templates/index.html'), name='template-panel'),
	url(r'^blank-page', AuthenticatedTemplateView.as_view(template_name='angular_templates/blank-page.html'), name='template-blank-page'),	
)
