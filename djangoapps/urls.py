from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

from dashboard.views import index

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djangoapps.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^dashboard/', include('dashboard.urls')),

     # Index e Home. Se a aplicacao tiver um site externo ao dashboard
     # talvez seja necessario alterar essas URLS.
     # notar que deve existir uma URL com nome 'home'
	url(r'^home/', TemplateView.as_view(template_name='home.html'), name='home'),
	url(r'^', TemplateView.as_view(template_name='home.html')),
)
